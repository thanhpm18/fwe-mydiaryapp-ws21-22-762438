import "reflect-metadata";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";
import fetch from "node-fetch";

describe("diary", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("should be able to get financial news", async (done) => {
    const EURResponse = await fetch(
      `https://freecurrencyapi.net/api/v2/latest?apikey=0bec0700-4dfa-11ec-9924-d3e40b643de7&base_currency=eur`,
      {
        headers: { "content-type": "application/json" },
        method: "GET",
      }
    );
    const BTCResponse = await fetch(`https://api.alternative.me/v1/ticker/bitcoin/?convert=EUR`, {
      headers: { "content-type": "application/json" },
      method: "GET",
    });
    const ETHResponse = await fetch(`https://api.alternative.me/v1/ticker/ethereum/?convert=EUR`, {
      headers: { "content-type": "application/json" },
      method: "GET",
    });
    const EURjson = await EURResponse.json();
    const BTCjson = await BTCResponse.json();
    const ETHjson = await ETHResponse.json();
    request(helper.app)
      .get("/api/financial")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.BTC_EUR).toBe(Number(Number(BTCjson[0].price_eur).toFixed(3)));
        expect(res.body.BTC_USD).toBe(Number(BTCjson[0].price_usd));

        expect(res.body.ETH_EUR).toBe(Number(Number(ETHjson[0].price_eur).toFixed(3)));
        expect(res.body.ETH_USD).toBe(Number(Number(ETHjson[0].price_usd)));

        
        expect(res.body.EUR_USD).toBe(Number(Number(EURjson.data.USD)));
        expect(res.body.EUR_VND).toBe(Number(Number(EURjson.data.VND).toFixed(2)));
        
        done();
      });
  });
});

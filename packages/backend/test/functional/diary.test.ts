import "reflect-metadata";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";
import { Diary } from "../../src/entity/diary";
import { Label } from "../../src/entity/label";

describe("diary", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("should be able to get all diaries", async (done) => {
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";

    const savedDiary = await helper.getRepo(Diary).save(diary);
    request(helper.app)
      .get("/api/diary")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.length).toBe(1);
        expect(res.body.data[0].id).toBe(savedDiary.id);
        expect(res.body.data[0].name).toBe(savedDiary.name);
        expect(res.body.data[0].text).toBe(savedDiary.text);
        expect(res.body.data[0].imgUrl).toBe(savedDiary.imgUrl);
        done();
      });
  });

  it("it should be able to create a new diary without labels", async (done) => {
    await helper.resetDatabase();
    request(helper.app)
      .post("/api/diary")
      .send({
        imgUrl: "https://www.google.de/",
        name: "Testwert",
        text: "** test Text **",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testwert");
        expect(res.body.data.imgUrl).toBe("https://www.google.de/");
        expect(res.body.data.text).toBe("** test Text **");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.labels).toBeDefined();
        done();
      });
  });

  it("it should be able to create a new diary with labels", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Saved Label";
    await helper.getRepo(Label).save(label);

    request(helper.app)
      .post("/api/diary")
      .send({
        imgUrl: "https://www.google.de/",
        labels: [{ name: "Testlabel" }, { name: "Saved Label" }],
        name: "Testwert",
        text: "** test Text **",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testwert");
        expect(res.body.data.imgUrl).toBe("https://www.google.de/");
        expect(res.body.data.text).toBe("** test Text **");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.labels).toBeDefined();
        expect(res.body.data.labels[0].name).toBe("Testlabel");
        expect(res.body.data.labels[1].name).toBe("Saved Label");
        done();
      });
  });

  it("it should be able to delete a diary", async (done) => {
    await helper.resetDatabase();
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";
    const savedDiary = await helper.getRepo(Diary).save(diary);
    request(helper.app)
      .delete(`/api/diary/${savedDiary.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err) => {
        if (err) throw err;
        const [, diaryCount] = await helper.getRepo(Diary).findAndCount();
        expect(diaryCount).toBe(0);
        done();
      });
  });

  it("it should be able to update a diary", async (done) => {
    await helper.resetDatabase();
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/"
    diary.labels = [];
    const savedDiary = await helper.getRepo(Diary).save(diary);

    const label1 = new Label();
    label1.name = "Testlabel1";
    const savedLabel = await helper.getRepo(Label).save(label1);

    const label2 = new Label();
    label2.name = "Testlabel2";

    request(helper.app)
      .patch(`/api/diary/${savedDiary.id}`)
      .send({
        imgUrl: "edited URL",
        labels: [savedLabel, label2],
        name: "Edited Name",
        text: "** Edited Text **",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Edited Name");
        expect(res.body.data.imgUrl).toBe("edited URL");
        expect(res.body.data.text).toBe("** Edited Text **");
        expect(res.body.data.labels.length).toBe(2);
        expect(res.body.data.labels[0].name).toBe(label1.name);
        expect(res.body.data.labels[1].name).toBe(label2.name);
        done();
      });
  });

  it("it should be able to get a single diary", async (done) => {
    await helper.resetDatabase();
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";
    const savedDiary = await helper.getRepo(Diary).save(diary);
    request(helper.app)
      .get(`/api/diary/${savedDiary.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.id).toBe(savedDiary.id);
        expect(res.body.data.name).toBe(savedDiary.name);
        expect(res.body.data.text).toBe(savedDiary.text);
        expect(res.body.data.imgUrl).toBe(savedDiary.imgUrl);
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        done();
      });
  });
});

import "reflect-metadata";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";
import { Label } from "../../src/entity/label";
import { Diary } from "../../src/entity/diary";

describe("label", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("should be able to get all labels", async (done) => {
    const label = new Label();
    label.name = "Testlabel";

    const savedLabel = await helper.getRepo(Label).save(label);
    request(helper.app)
      .get("/api/label")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.length).toBe(1);
        expect(res.body.data[0].id).toBe(savedLabel.id);
        expect(res.body.data[0].name).toBe(savedLabel.name);
        done();
      });
  });

  it("it should be able to create a new label", async (done) => {
    await helper.resetDatabase();
    request(helper.app)
      .post("/api/label")
      .send({
        name: "Testlabel",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testlabel");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        done();
      });
  });

  it("it should not be able to create a new label with an already existing name", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";

    await helper.getRepo(Label).save(label);
    request(helper.app)
      .post("/api/label")
      .send({
        name: "Testlabel",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.status).toBe(400);
        expect(res.body.status).toBe("bad_request");
        done();
      });
  });

  it("it should be able to delete a label", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);
    request(helper.app)
      .delete(`/api/label/${savedLabel.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err) => {
        if (err) throw err;
        const [, labelCount] = await helper.getRepo(Label).findAndCount();
        expect(labelCount).toBe(0);
        done();
      });
  });

  it("it should be able to update a label", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);
    request(helper.app)
      .patch(`/api/label/${savedLabel.id}`)
      .send({
        name: "Edited Name",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Edited Name");
        done();
      });
  });

  it("it should not be able to update a label with an already existing name", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);
    const label2 = new Label();
    label2.name = "Edited Name";
    await helper.getRepo(Label).save(label2);
    request(helper.app)
      .patch(`/api/label/${savedLabel.id}`)
      .send({
        name: "Edited Name",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.status).toBe(400);
        expect(res.body.status).toBe("bad_request");
        done();
      });
  });

  it("it should be able to get a single label", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);
    request(helper.app)
      .get(`/api/label/${savedLabel.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.id).toBe(savedLabel.id);
        expect(res.body.data.name).toBe(savedLabel.name);
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        done();
      });
  });

  it("it should be able to get all diaries of single label", async (done) => {
    await helper.resetDatabase();
    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);

    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";
    diary.labels = [savedLabel];

    const savedDiary = await helper.getRepo(Diary).save(diary);

    request(helper.app)
      .get(`/api/label/${savedLabel.id}/diary`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.length).toBe(1);
        expect(res.body.data[0].id).toBe(savedDiary.id);
        expect(res.body.data[0].name).toBe(savedDiary.name);
        expect(res.body.data[0].imgUrl).toBe(savedDiary.imgUrl);
        expect(res.body.data[0].text).toBe(savedDiary.text);
        done();
      });
  });
});

import "reflect-metadata";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";
import { Label } from "../../src/entity/label";
import { Diary } from "../../src/entity/diary";

describe("diary_label", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("it should be able to add a label to a diary", async (done) => {
    await helper.resetDatabase();
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";
    const savedDiary = await helper.getRepo(Diary).save(diary);

    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);

    request(helper.app)
      .post(`/api/diary/${savedDiary.id}/label/${savedLabel.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testdiary");
        expect(res.body.data.imgUrl).toBe("https://www.google.de/");
        expect(res.body.data.text).toBe("** test Text **");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.labels).toBeDefined();
        expect(res.body.data.labels[0].name).toBe("Testlabel");
        done();
      });
  });

  it("it should be able to delete a label from a diary", async (done) => {
    await helper.resetDatabase();
    const diary = new Diary();
    diary.name = "Testdiary";
    diary.text = "** test Text **";
    diary.imgUrl = "https://www.google.de/";
    diary.labels = [];

    const label = new Label();
    label.name = "Testlabel";
    const savedLabel = await helper.getRepo(Label).save(label);

    diary.labels.push(savedLabel);
    const savedDiary = await helper.getRepo(Diary).save(diary);

    request(helper.app)
      .delete(`/api/diary/${savedDiary.id}/label/${savedLabel.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testdiary");
        expect(res.body.data.imgUrl).toBe("https://www.google.de/");
        expect(res.body.data.text).toBe("** test Text **");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.labels).toBeDefined();
        expect(res.body.data.labels.length).toBe(0);
        done();
      });
  });
});

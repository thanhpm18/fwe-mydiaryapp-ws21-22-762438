import { getRepository } from "typeorm";
import { Request, Response } from "express";
import { Label } from "../entity/label";
import { Diary } from "../entity/diary";

/**
 * Get all labels in the database
 * 
 * @param {Request} _ Request
 * @param {Response} res Response
 */
export const getAllLabels = async (_: Request, res: Response) => {
  const labelRepository = await getRepository(Label);
  const labels = await labelRepository.find();
  res.send({
    data: labels,
  });
};

/**
 * Create new label
 * Return 400 if label name already exists
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const createLabel = async (req: Request, res: Response) => {
  const { name } = req.body;

  const label = new Label();
  label.name = name;

  const labelRepository = await getRepository(Label);
  try {
    const createdLabel = await labelRepository.save(label);
    res.send({
      data: createdLabel,
    });
  } catch (error) {
    res.status(400).send({
      status: "bad_request",
    });
  }
};

/**
 * Get a single label by ID
 * Return 404 if label not found
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const getLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const labelRepository = await getRepository(Label);
  try {
    const label = await labelRepository.findOneOrFail(labelId);
    res.send({
      data: label,
    });
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

/**
 * Delete a label
 * Return 404 if label not found
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const deleteLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const labelRepository = await getRepository(Label);
  try {
    const label = await labelRepository.findOneOrFail(labelId);
    await labelRepository.remove(label);

    res.send({});
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

/**
 * Update a label
 * Return 404 if label not found
 * Return 400 if label name already exists
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const patchLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const { name } = req.body;
  const labelRepository = await getRepository(Label);
  try {
    let label = await labelRepository.findOneOrFail(labelId);
    label.name = name;

    try {
      label = await labelRepository.save(label);
      res.send({
        data: label,
      });
    } catch (error) {
      res.status(400).send({
        status: "bad_request",
      });
    }
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

/**
 * Get all diaries from a label in the database
 * Return 404 if label not found
 * 
 * @param {Request} _ Request
 * @param {Response} res Response
 */
export const getAllDiariesFromLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const labelRepository = await getRepository(Label);
  const diaryRepository = await getRepository(Diary);

  try {
    await labelRepository.findOneOrFail(labelId);

    try {
      const diaries = await diaryRepository
        .createQueryBuilder("diary")
        .innerJoin("diary.labels", "label", "label.id = :id", { id: labelId })
        .getMany();
      
      res.send({
        data: diaries,
      });
    } catch (error) {
      res.status(404).send({
        status: error,
      });
    }
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

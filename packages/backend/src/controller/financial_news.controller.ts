import { Request, Response } from "express";
import fetch from "node-fetch";
/**
 * Get financial data from the external APIs
 * Return errors if can not fetch data from external APIs
 * 
 * @param {Request} _ Request
 * @param {Response} res Response
 */
export const getFinancialNews = async (_: Request, res: Response) => {
  try {
    const EURResponse = await fetch(
      `https://freecurrencyapi.net/api/v2/latest?apikey=0bec0700-4dfa-11ec-9924-d3e40b643de7&base_currency=eur`,
      {
        headers: { "content-type": "application/json" },
        method: "GET",
      }
    );
    const BTCResponse = await fetch(`https://api.alternative.me/v1/ticker/bitcoin/?convert=EUR`, {
      headers: { "content-type": "application/json" },
      method: "GET",
    });
    const ETHResponse = await fetch(`https://api.alternative.me/v1/ticker/ethereum/?convert=EUR`, {
      headers: { "content-type": "application/json" },
      method: "GET",
    });
    const EURjson = await EURResponse.json();
    const BTCjson = await BTCResponse.json();
    const ETHjson = await ETHResponse.json();
    res.send({
      BTC_EUR: Number(Number(BTCjson[0].price_eur).toFixed(3)),
      BTC_USD: Number(Number(BTCjson[0].price_usd)),
      ETH_EUR: Number(Number(ETHjson[0].price_eur).toFixed(3)),
      ETH_USD: Number(Number(ETHjson[0].price_usd)),
      EUR_USD: Number(Number(EURjson.data.USD)),
      EUR_VND: Number(Number(EURjson.data.VND).toFixed(2)),
      timestamp: EURjson.query.timestamp*1000,
    });
  } catch (e) {
    res.send({
      error: e
    });
  }
};

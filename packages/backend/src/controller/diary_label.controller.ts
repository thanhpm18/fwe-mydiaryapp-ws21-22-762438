import { Request, Response, NextFunction } from "express";
import { Repository, getRepository } from "typeorm";
import { Diary } from "../entity/diary";
import { Label } from "../entity/label";

export class DiaryLabelController {
  /**
   * Add a given label by id to a diary by id
   * Return 404 if label not found
   * Return 404 if diary not found
   * - check if Label and diary exists
   * - check if the label is already added to the diary
   * - if label is not added, save it
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async addLabelToDiary(req: Request, res: Response, _: NextFunction): Promise<void> {
    const labelId: number = Number(req.params.labelId);
    const diaryId: number = Number(req.params.diaryId);

    const diaryRepository: Repository<Diary> = getRepository(Diary);
    const labelRepository: Repository<Label> = getRepository(Label);

    try {
      const label: Label = await labelRepository.findOneOrFail(labelId);
      let diary: Diary = await diaryRepository.findOneOrFail(diaryId);

      // Check if label already exists in diary
      if (!diary.labels.some((addedLabel: Label) => addedLabel.id === labelId)) {
        diary.labels.push(label);
        diary = await diaryRepository.save(diary);
      }

      res.send({ status: "ok", data: diary });
    } catch (e) {
      res.status(404).send({ status: "not_found" });
    }
  }

  /**
   * Remove a given label by id from a diary by id
   * - Returns 404 if diary not found
   * - Filter all labels and return only the labels that don't have the labelId that should be removed
   * - Save updated array to remove the label from the jointable
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async removeLabelFromDiary(req: Request, res: Response, _: NextFunction): Promise<void> {
    const labelIdToRemove: number = Number(req.params.labelId);
    const diaryId: string = req.params.diaryId;

    const diaryRepository: Repository<Diary> = getRepository(Diary);

    try {
      let diary: Diary = await diaryRepository.findOneOrFail(diaryId);
      diary.labels = diary.labels.filter((diaryLabel: Label) => diaryLabel.id !== labelIdToRemove);
      diary = await diaryRepository.save(diary);
      res.send({ status: "ok", data: diary });
    } catch (e) {
      res.status(404).send({ status: "not_found" });
    }
  }
}

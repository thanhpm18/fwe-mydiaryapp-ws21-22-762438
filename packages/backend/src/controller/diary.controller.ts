import { getRepository } from "typeorm";
import { Request, Response } from "express";
import { Diary } from "../entity/diary";
import { Label } from "../entity/label";

/**
 * Get all diaries in the database
 * Sort by ID Descending
 * 
 * @param {Request} _ Request
 * @param {Response} res Response
 */
export const getAllDiaries = async (_: Request, res: Response) => {
  const diaryRepository = await getRepository(Diary);
  const diaries = await diaryRepository.find({
    order: {
      id: "DESC",
    },
  });
  res.send({
    data: diaries,
  });
};

/**
 * Create new diary
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const createDiary = async (req: Request, res: Response) => {
  const { name, text, imgUrl, labels } = req.body;

  const diary = new Diary();
  diary.name = name;
  diary.text = text;
  diary.imgUrl = imgUrl;
  diary.labels = [];

  const diaryRepository = await getRepository(Diary);
  let createdDiary = await diaryRepository.save(diary);

  const labelRepository = await getRepository(Label);
  if (labels) {
    for (const requestedLabel of labels) {
      const label = await labelRepository.find({ name: requestedLabel.name });
      if (label[0]) {
        if (!diary.labels.some((addedLabel: Label) => addedLabel.id === label[0].id)) {
          createdDiary.labels.push(label[0]);
        }
      } else {
        const newLabel = new Label();
        newLabel.name = requestedLabel.name;
        const createdLabel = await labelRepository.save(newLabel);
        createdDiary.labels.push(createdLabel);
      }
    }
  }
  createdDiary = await diaryRepository.save(createdDiary);
  res.send({
    data: createdDiary,
  });
};

/**
 * Get a single diary by ID
 * Return 404 if diary not found
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const getDiary = async (req: Request, res: Response) => {
  const diaryId = req.params.diaryId;
  const diaryRepository = await getRepository(Diary);
  try {
    const diary = await diaryRepository.findOneOrFail(diaryId);
    res.send({
      data: diary,
    });
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

/**
 * Delete a diary
 * Return 404 if diary not found
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const deleteDiary = async (req: Request, res: Response) => {
  const diaryId = req.params.diaryId;
  const diaryRepository = await getRepository(Diary);
  try {
    const diary = await diaryRepository.findOneOrFail(diaryId);
    await diaryRepository.remove(diary);

    res.send({});
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

/**
 * Update a diary
 * Return 404 if diary not found
 * 
 * @param {Request} req Request
 * @param {Response} res Response
 */
export const patchDiary = async (req: Request, res: Response) => {
  const diaryId = req.params.diaryId;
  const { name, text, imgUrl, labels } = req.body;
  const diaryRepository = await getRepository(Diary);

  const labelRepository = await getRepository(Label);
  try {
    let diary = await diaryRepository.findOneOrFail(diaryId);
    diary.name = name;
    diary.text = text;
    diary.imgUrl = imgUrl;
    diary.labels = [];

    if (labels) {
      for (const requestedLabel of labels) {
        const label = await labelRepository.find({ name: requestedLabel.name });
        if (label[0]) {
          if (!diary.labels.some((addedLabel: Label) => addedLabel.id === label[0].id)) {
            diary.labels.push(label[0]);
          }
        } else {
          const newLabel = new Label();
          newLabel.name = requestedLabel.name;
          const createdLabel = await labelRepository.save(newLabel);
          diary.labels.push(createdLabel);
        }
      }
    }

    diary = await diaryRepository.save(diary);

    res.send({
      data: diary,
    });
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

import { Router } from "express";
import {
  createLabel,
  deleteLabel,
  getAllDiariesFromLabel,
  getAllLabels,
  getLabel,
  patchLabel,
} from "../controller/label.controller";

/** Variables */
export const labelRouter = Router({ mergeParams: true });

/** Routes */
labelRouter.get("/", getAllLabels);

labelRouter.post("/", createLabel);

labelRouter.get("/:labelId", getLabel);

labelRouter.delete("/:labelId", deleteLabel);

labelRouter.patch("/:labelId", patchLabel);

labelRouter.get("/:labelId/diary", getAllDiariesFromLabel);

import { Router } from "express";
import { getFinancialNews } from "../controller/financial_news.controller";

/** Variables */
export const finalcialNewsRouter = Router({ mergeParams: true});

/** Routes */
finalcialNewsRouter.get('/', getFinancialNews);
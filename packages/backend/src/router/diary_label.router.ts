import { Router } from 'express';
import { DiaryLabelController } from '../controller/diary_label.controller';

/** Variables */
export const diaryLabelRouter: Router = Router({ mergeParams: true });

/** Routes */
diaryLabelRouter.post('/:labelId', DiaryLabelController.addLabelToDiary);
diaryLabelRouter.delete('/:labelId', DiaryLabelController.removeLabelFromDiary);

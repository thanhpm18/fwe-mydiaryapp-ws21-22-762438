import { Request, Response, Router } from "express";
import { diaryRouter } from "./diary.router";
import { finalcialNewsRouter } from "./financial_news.router";
import { labelRouter } from "./label.router";

/** Variables */
export const globalRouter = Router({ mergeParams: true });

/** Routes */
globalRouter.get("/", async (_: Request, res: Response) => {
  console.log("Hallo welt");
  res.send({
    message: "Hello",
  });
});

globalRouter.use("/diary", diaryRouter);
globalRouter.use("/label", labelRouter);
globalRouter.use("/financial", finalcialNewsRouter);

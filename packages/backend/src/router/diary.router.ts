import { Router } from "express";
import { createDiary, deleteDiary, getAllDiaries, getDiary, patchDiary } from "../controller/diary.controller";
import { diaryLabelRouter } from "./diary_label.router";

/** Variables */
export const diaryRouter = Router({ mergeParams: true });

/** Routes */
diaryRouter.get('/', getAllDiaries);

diaryRouter.post('/', createDiary);

diaryRouter.get('/:diaryId', getDiary);

diaryRouter.delete('/:diaryId', deleteDiary);

diaryRouter.patch('/:diaryId', patchDiary);

/** Transaction-Tag routes */
diaryRouter.use('/:diaryId/label', diaryLabelRouter);

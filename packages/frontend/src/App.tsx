import React, { useEffect } from "react";
// import logo from './logo.svg';
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { GlobalStyle } from "./components/GlobalStyle";
import { theme } from "./theme";
import { ThemeProvider } from "styled-components";
import { DiaryPage } from "./pages/Diary/DiaryPage";
import { LabelPage } from "./pages/Label/LabelPage";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { ShowDiaryPage } from "./pages/Diary/components/ShowDiaryPage";
import { ShowLabelPage } from "./pages/Label/components/ShowLabelPage";
import { FinancialNewsPage } from "./pages/FinancialNewsPage";
import { HomePage } from "./pages/HomePage";

library.add(fas, fab, far);

function App() {
  useEffect(() => {
    (async function () {
      const helloRequest = await fetch("/api");
      const helloJson = await helloRequest.json();
      console.log(helloJson);
    })();
  });
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/diary" element={<DiaryPage />} />
          <Route path="/label" element={<LabelPage />} />
          <Route path="/diary/:diaryId" element={<ShowDiaryPage />} />
          <Route path="/label/:labelId" element={<ShowLabelPage />} />
          <Route path="/financial" element={<FinancialNewsPage />} />
        </Routes>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;

import styled from "styled-components";

export const CSVButtonHolder = styled.div`
  width: 48px;
  border: 0px;
  height: 48px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background-color: ${(props) => props.theme.colors.primary};
  cursor: pointer;
  color: ${(props) => props.theme.colors.backgroundColor};
  margin-left: 15px;
  margin-bottom: 15px;
`;

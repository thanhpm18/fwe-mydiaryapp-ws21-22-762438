import styled from "styled-components/macro";

export const FormErrorHolder = styled.p`
  color: red;
  text-align: center;
`;
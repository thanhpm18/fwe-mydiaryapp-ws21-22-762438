import React from "react";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components/macro";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const headerHeight = "85px";
const footerHeight = "50px";

export const MaxWidthCSS = css`
  max-width: 860px;
  margin: auto;
`;
const Header = styled.header`
  height: ${headerHeight};
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 25px;
  border-bottom: 2px solid ${(props) => props.theme.colors.primary};
  position: sticky;
  top: 0;
  background-color: ${(props) => props.theme.colors.backgroundColor};
  z-index: 2;
`;

const Main = styled.main`
  min-height: calc(100vh - ${headerHeight} - ${footerHeight});
  padding: 0px 25px;
  ${MaxWidthCSS}
  z-index: 1;
`;

const Footer = styled.footer`
  height: ${footerHeight};
  padding: 0 25px;
  ${MaxWidthCSS};
  margin-top: 25px;
`;

const NavigationList = styled.ul`
  list-style: none;
`;
const NavigationItem = styled.li`
  color: ${(props) => props.theme.colors.primary};
`;

const SideBar = styled.div`
  width: 18%;
  height: calc(100vh - ${headerHeight} + 20px);
  display: flex;
  flex-direction: column;
  position: fixed;
  top: ${headerHeight};
  border-right: 2px solid ${(props) => props.theme.colors.primary};
  min-width: 200px;
`;

const SideBarItem = styled.h2`
  text-align: center;
  border-bottom: 2px solid ${(props) => props.theme.colors.primary};
  margin: 0px;
  padding: 15px;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.theme.colors.primary};
    color: ${(props) => props.theme.colors.backgroundColor};
  }
`;

export const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Header>
        <div
          css={`
            font-size: 25px;
            letter-spacing: 2.3px;
            flex: 1;
            display: flex;
            flex-direction: row;
          `}
        >
          <span
            css={`
              text-decoration: underline overline;
            `}
          >
            MY diary
          </span>
        </div>
        <NavigationList>
          <NavigationItem>
            <Link
              css={`
                all: unset;
                cursor: pointer;
              `}
              to={"/"}
            >
              Home
            </Link>
          </NavigationItem>
        </NavigationList>
      </Header>
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <SideBar>
          <Link
            to={"/diary"}
            css={`
              all: unset;
            `}
          >
            <SideBarItem>
              <FontAwesomeIcon
                icon={["fas", "book"]}
                css={`
                  margin-right: 5px;
                `}
              />
              Diary
            </SideBarItem>
          </Link>
          <Link
            to={"/label"}
            css={`
              all: unset;
            `}
          >
            <SideBarItem>
              <FontAwesomeIcon
                icon={["fas", "tags"]}
                css={`
                  margin-right: 5px;
                `}
              />
              All Labels
            </SideBarItem>
          </Link>
          <Link
            to={"/financial"}
            css={`
              all: unset;
            `}
          >
            <SideBarItem>
              <FontAwesomeIcon
                icon={["fas", "money-bill-alt"]}
                css={`
                  margin-right: 5px;
                `}
              />
              Financial News
            </SideBarItem>
          </Link>
        </SideBar>
        <div
          css={`
            flex: 1;
            margin-left: 230px;
          `}
        >
          <Main>{children}</Main>
          <Footer>© 2021 FWE</Footer>
        </div>
      </div>
    </>
  );
};

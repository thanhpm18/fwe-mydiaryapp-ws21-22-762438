import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ModalHolder = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  height: 100vh;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 2;
  overflow-y: auto;
`;

export const ModalMask = styled.div`
  border-radius: 8px;
  padding: 20px 16px;
  box-shadow: 0 4px 5px rgba(0, 0, 0, 0.075);
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
  min-height: fit-content;
  width: 500px;
  min-width: 380px;
  line-height: 25.2px;
`;
export const ModalMaskHolder = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: inherit;
  height: 100%;
  color: #000;
`;

const ModalHeader = styled.div`
  display: flex;
  width: 100%;
`;

const ModalTitle = styled.h2`
  flex: 1;
  margin: 10px 0;
`;

const ModalCloseButton = styled.button`
  all: unset;
  color: #000;
  cursor: pointer;
`;

export const Modal: React.FC<{
  title: string;
  onCancel: () => void;
}> = ({ children, title, onCancel }) => {
  const modalRoot = document.getElementById("modal-root");
  return ReactDOM.createPortal(
    <ModalHolder>
      <ModalMaskHolder>
        <ModalMask>
          <ModalHeader>
            <ModalTitle>{title}</ModalTitle>
            <ModalCloseButton onClick={onCancel}><FontAwesomeIcon icon={['fas', 'times']}/></ModalCloseButton>
          </ModalHeader>
          {children}
        </ModalMask>
      </ModalMaskHolder>
    </ModalHolder>,
    modalRoot!
  );
};

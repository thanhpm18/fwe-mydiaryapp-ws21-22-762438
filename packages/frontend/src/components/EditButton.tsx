import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const EditButton = (props: React.ButtonHTMLAttributes<HTMLButtonElement>) => {
  const StyledButton = styled.button`
    all: unset;
    /* width: 48px;
    border: 0px;
    height: 48px; */
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: 10px;
    /* border-radius: 50%; */
    /* background-color: ${(props) => props.theme.colors.primary}; */
    cursor: pointer;
    color: ${(props) => props.theme.colors.secondaryFontColor};
    &:hover{
      color: ${(props) => props.theme.colors.primary};
    }
  `;
  return (
    <StyledButton {...props}>
      <FontAwesomeIcon icon={["fas", "pencil-alt"]} size='lg'/>
    </StyledButton>
  );
};

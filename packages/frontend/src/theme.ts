import { DefaultTheme } from "styled-components";

export const theme: DefaultTheme = {
  colors: {
    primary: "rgb(97, 219, 251)",
    backgroundColor: "#202020",
    fontColor: "#fff",
    secondaryFontColor: "rgb(191, 191, 191)",
    shadowColor: "rgba(0, 0, 0, 0.3)",
    listBackgroundColor: "rgb(45, 45, 45)",
    dangerButtonColor: "#ff5252",
  },
};

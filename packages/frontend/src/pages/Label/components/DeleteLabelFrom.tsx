import React from "react";
import { Button } from "../../../components/Button";
import { Label } from "./LabelList";
import { DangerButton } from "../../../components/DangerButton";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styled from "styled-components/macro";
// import { labelContext } from "../../../contexts/LabelContext";

export const DeleteLabelForm: React.FC<{ afterSubmit: () => void; onCancel: () => void; label: Label }> = ({
  afterSubmit,
  onCancel,
  label,
}) => {
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    await fetch(`/api/label/${label.id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    afterSubmit();
  };
  return (
    <form onSubmit={onSubmitForm}>
      <h3>Do you really want to delete this?</h3>
      <h4>Label: {label.name}</h4>
      <h5>Created at: {new Date(label.createdAt).toLocaleString("de-DE")}</h5>
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <Button type="button" onClick={onCancel}>Cancel</Button>
        <DangerButton type="submit">Delete Label</DangerButton>
      </div>
    </form>
  );
};

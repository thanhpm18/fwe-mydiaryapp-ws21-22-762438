import React from "react";
import styled from "styled-components";

export type Label = {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
};

/* export type Diary = {
  id: string;
  name: string;
  text: string;
  imgUrl: string;
  createdAt: Date;
  updatedAt: Date;
  labels: Label[];
}; */

/* const LabelList = styled.ul`
  list-style: none;
  flex-grow: 1;
  font-size: 0.8rem;

  align-self: flex-end;
  display: flex;
  & > li {
    margin-right: 0.5rem;
    padding: 0.125rem;
    border-radius: 0.25rem;
    background-color: ${(props) => props.theme.colors.primary}
    display: block;
    color: #fff;
  }
`; */

const LabelFlex = styled.div`
  display: flex;
  align-items: center;
`;

export const LabelHighlight = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: none;
  width: 4px;
  background-color: ${(props) => props.theme.colors.primary};
`;

export const LabelItemStyle = styled.div`
  margin: 0;
  min-height: 3rem;
  position: relative;
  padding: 0.7rem 2rem;
  &:hover {
    ${LabelHighlight} {
      display: block;
    }
  }
`;
export const LabelList = styled.ul`
  list-style: none;
  box-shadow: 0 0.125em 0.25em 0 ${(props) => props.theme.colors.shadowColor};
  width: 100%;
  min-width: 380px;
  padding: 0;
  border-radius: 0.5rem;
  background-color: ${(props) => props.theme.colors.listBackgroundColor};
  ${LabelItemStyle} {
    border-bottom: 1px ${(props) => props.theme.colors.shadowColor} solid;
    &:last-of-type {
      border-bottom: 0;
    }
  }
`;

export const LabelName = styled.p`
  font-size: 1.1rem;
  font-weight: 500;
  margin: 0;
`;

export const LabelText = styled.p`
  font-size: 0.8rem;
  margin: 0;
`;
export const LabelDate = styled.p`
  margin: 0;
  font-size: 0.8rem;
  color: ${(props) => props.theme.colors.secondaryFontColor};
`;
export const LabelImgUrl = styled.span`
  white-space: nowrap;
`;
export type LabelItemProps = {
  label: Label;
};

export const LabelItem: React.FC<LabelItemProps> = ({
  label: { name, createdAt }, children
}) => {
  return (
    <LabelItemStyle>
      <LabelHighlight />
      <LabelFlex>
        <div>
          <LabelName>{name}</LabelName>
          <LabelDate>
            {createdAt && new Date(createdAt).toLocaleString("de-DE")}
          </LabelDate>
        </div>
        {children}
      </LabelFlex>
    </LabelItemStyle>
  );
};

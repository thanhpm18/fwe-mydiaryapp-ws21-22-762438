import React, { useState, ChangeEvent } from "react";
import { Input } from "../../../components/Input";
import { Button } from "../../../components/Button";
import { FormErrorHolder } from "../../../components/FormErrorHolder";

export const AddLabelForm: React.FC<{ afterSubmit: () => void }> = ({ afterSubmit }) => {
  const [values, setValues] = useState({
    name: "",
  });
  const [formError, setFormError] = useState<string | null>(null);
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setFormError(null);
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(values);
    setFormError(null);

    const postRequest = await fetch("/api/label", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        ...values,
      }),
    });
    if (postRequest.status === 200) {
      afterSubmit();
    } else {
      if (postRequest.status === 400) {
        setFormError("Bad Request: that name already exists! Please try another one");
      }
      else {
        setFormError("Error " + postRequest.status + ": " +  postRequest.statusText);
      }
    }
  };
  return (
    <form onSubmit={onSubmitForm}>
      <FormErrorHolder>{formError}</FormErrorHolder>
      <Input name="name" type="text" label="Name" onChange={fieldDidChange} required />
      <Button type="submit">Add Label</Button>
    </form>
  );
};

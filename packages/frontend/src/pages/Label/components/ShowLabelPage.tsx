import React, { useEffect, useState } from "react";
import { Label, LabelDate } from "./LabelList";
import { Button } from "../../../components/Button";
import styled from "styled-components/macro";
import { DangerButton } from "../../../components/DangerButton";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Diary, DiaryItem, DiaryList } from "../../Diary/components/DiaryList";
import { ShowButton } from "../../../components/ShowButton";
import { Modal } from "../../../components/Modal";
import { EditLabelForm } from "./EditLabelFrom";
import { DeleteLabelForm } from "./DeleteLabelFrom";
import { Layout } from "../../../components/Layout";

const CRUDButtonHolder = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0;
  right: 0;
  margin: 15px;
`;

export const ShowLabelPage = () => {
  const labelId = useParams().labelId;
  const [label, setLabel] = useState<Label>();

  const [diaries, setDiaries] = useState<Diary[]>([]);

  const [editLabelVisible, setEditLabelVisible] = useState(false);
  const [deleteLabelVisible, setDeleteLabelVisible] = useState(false);
  const navigate = useNavigate();

  const fetchLabel = async () => {
    const labelRequest = await fetch(`/api/label/${labelId}`, {
      headers: { "content-type": "application/json" },
    });
    console.log(labelRequest);
    if (labelRequest.status === 200) {
      const labelJSON = await labelRequest.json();
      setLabel(labelJSON.data);
    }
  };

  useEffect(() => {
    (async function () {
      const labelRequest = await fetch(`/api/label/${labelId}`, {
        headers: { "content-type": "application/json" },
      });
      console.log(labelRequest);
      if (labelRequest.status === 200) {
        const labelJSON = await labelRequest.json();
        setLabel(labelJSON.data);
      }
    })();
  }, [labelId]);

  const fetchDiaries = async function () {
    const diaryRequest = await fetch(`/api/label/${labelId}/diary`, {
      headers: { "content-type": "application/json" },
    });
    console.log(diaryRequest);
    if (diaryRequest.status === 200) {
      const diaryJSON = await diaryRequest.json();
      setDiaries(diaryJSON.data);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    (async function () {
      await fetchDiaries();
    })();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Layout>
      <h1>{label && label.name}</h1>
      <LabelDate>{label && label.createdAt && new Date(label.createdAt).toLocaleString("de-DE")}</LabelDate>
      <h2>Diary of Label: {diaries.length}</h2>
      <DiaryList>
        {diaries.map((diary) => (
          <DiaryItem diary={diary} labelInvisible={false}>
            <CRUDButtonHolder>
              <Link to={`/diary/${diary.id}`}>
                <ShowButton />
              </Link>
            </CRUDButtonHolder>
          </DiaryItem>
        ))}
      </DiaryList>
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <Button
          onClick={() => {
            setEditLabelVisible(true);
          }}
        >
          Edit Label
        </Button>
        <DangerButton
          onClick={() => {
            setDeleteLabelVisible(true);
          }}
        >
          Delete Label
        </DangerButton>
      </div>
      {editLabelVisible && label && (
        <Modal
          title="Edit Label"
          onCancel={() => {
            setEditLabelVisible(false);
          }}
        >
          <EditLabelForm
            afterSubmit={() => {
              setEditLabelVisible(false);
              fetchLabel();
            }}
            onDelete={() => {
              setDeleteLabelVisible(true);
            }}
            label={label}
          />
        </Modal>
      )}
      {deleteLabelVisible && label && (
        <Modal
          title="Delete Label"
          onCancel={() => {
            setDeleteLabelVisible(false);
          }}
        >
          <DeleteLabelForm
            afterSubmit={() => {
              setEditLabelVisible(false);
              setDeleteLabelVisible(false);
              navigate("/label");
            }}
            onCancel={() => {
              setDeleteLabelVisible(false);
            }}
            label={label}
          />
        </Modal>
      )}
    </Layout>
  );
};

import React, { useState, ChangeEvent, useEffect } from "react";
import { Input } from "../../../components/Input";
import { Button } from "../../../components/Button";
import { Label } from "./LabelList";
import { DangerButton } from "../../../components/DangerButton";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styled from "styled-components/macro";
import { FormErrorHolder } from "../../../components/FormErrorHolder";

interface EditLabelFormState {
  name: string;
}

export const EditLabelForm: React.FC<{ afterSubmit: () => void; onDelete: () => void; label: Label }> = ({ afterSubmit, onDelete, label }) => {
  const [formError, setFormError] = useState<string | null>(null);

  const refetchLabels = async () => {
    const labelRequest = await fetch("/api/label", {
      headers: {
        "content-type": "application/json",
      },
    });
    const labelJson = await labelRequest.json();
    console.log(labelJson.data);
  };

  useEffect(() => {
    (async function () {
      await refetchLabels();
    })();
  }, []);
  const [values, setValues] = useState<EditLabelFormState>(label);
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setFormError(null);
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(values);
    setFormError(null);

    const patchRequest = await fetch(`/api/label/${label.id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        ...values,
      }),
    });
    if (patchRequest.status === 200) {
      await refetchLabels();
      afterSubmit();
    } else {
      if (patchRequest.status === 400) {
        setFormError("Bad Request: that name already exists! Please try another one");
      } else {
        setFormError("Error " + patchRequest.status + ": " + patchRequest.statusText);
      }
    }
  };
  return (
    <>
      <form onSubmit={onSubmitForm}>
        <FormErrorHolder>{formError}</FormErrorHolder>
        <Input name="name" type="text" label="Name" onChange={fieldDidChange} value={values.name} required />
        <div
          css={`
            display: flex;
            flex-direction: row;
          `}
        >
          <Button type="submit">Update Label</Button>
          <DangerButton type="button" onClick={onDelete}>Delete Label</DangerButton>
        </div>
      </form>
    </>
  );
};

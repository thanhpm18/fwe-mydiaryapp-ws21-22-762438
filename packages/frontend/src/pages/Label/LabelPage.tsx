import React, { useEffect, useState } from "react";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styled from "styled-components/macro";
import { LabelList, LabelItem, Label } from "./components/LabelList";
import { AddButton } from "./components/AddButton";
import { AddLabelForm } from "./components/AddLabelFrom";
import { Modal } from "../../components/Modal";
import { CSVButtonHolder } from "../../components/CSVButtonHolder";
import { CSVLink } from "react-csv";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { ShowButton } from "../../components/ShowButton";
import { EditButton } from "../../components/EditButton";
import { DeleteButton } from "../../components/DeleteButton";
import { EditLabelForm } from "./components/EditLabelFrom";
import { DeleteLabelForm } from "./components/DeleteLabelFrom";
import { Layout } from "../../components/Layout";

const CRUDButtonHolder = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0;
  right: 0;
  margin: 15px;
`;

export const LabelPage = () => {
  const [labels, setLabels] = useState<Label[]>([]);

  const [addLabelVisible, setAddLabelVisible] = useState(false);
  const [editLabelVisible, setEditLabelVisible] = useState(false);
  const [deleteLabelVisible, setDeleteLabelVisible] = useState(false);

  const [selectedLabel, setSelectedLabel] = useState<Label | null>(null);

  const csvData = labels.map((item) => ({
    id: item.id,
    name: item.name,
    createdAt: item.createdAt,
    updatedAt: item.updatedAt,
  }));
  const csvHeaders = [
    { label: "ID", key: "id" },
    { label: "Name", key: "name" },
    { label: "Created At", key: "createdAt" },
    { label: "Updated At", key: "updatedAt" },
  ];

  const fetchLabels = async function () {
    const labelRequest = await fetch("/api/label", {
      headers: { "content-type": "application/json" },
    });
    console.log(labelRequest);
    if (labelRequest.status === 200) {
      const labelJSON = await labelRequest.json();
      setLabels(labelJSON.data);
    }
  };

  useEffect(() => {
    (async function () {
      window.scrollTo(0, 0);
      const labelRequest = await fetch("/api/label", {
        headers: { "content-type": "application/json" },
      });
      console.log(labelRequest);
      if (labelRequest.status === 200) {
        const labelJSON = await labelRequest.json();
        setLabels(labelJSON.data);
      }
    })();
  }, []);

  return (
    <Layout>
      <div
        css={`
          display: flex;
          flex-direction: column;
          width: 100%;
        `}
      >
        <div>
          <h1>All Labels</h1>
        </div>
        <div
          css={`
            flex: 1;
            display: flex;
          `}
        >
          <AddButton
            onClick={() => {
              setAddLabelVisible(true);
            }}
          />
          <CSVButtonHolder>
            <CSVLink
              data={csvData}
              headers={csvHeaders}
              filename={"labels.csv"}
              separator={";"}
              css={`
                all: unset;
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
              `}
            >
              <FontAwesomeIcon icon={["fas", "file-csv"]} size="2x" />
            </CSVLink>
          </CSVButtonHolder>
        </div>
      </div>
      {addLabelVisible && (
        <Modal
          title="Create new Label"
          onCancel={() => {
            setAddLabelVisible(false);
          }}
        >
          <AddLabelForm
            afterSubmit={() => {
              setAddLabelVisible(false);
              fetchLabels();
            }}
          />
        </Modal>
      )}
      {editLabelVisible && selectedLabel && (
        <Modal
          title="Edit Label"
          onCancel={() => {
            setEditLabelVisible(false);
          }}
        >
          <EditLabelForm
            afterSubmit={() => {
              setEditLabelVisible(false);
              fetchLabels();
            }}
            onDelete={() => {
              setDeleteLabelVisible(true);
            }}
            label={selectedLabel}
          />
        </Modal>
      )}
      {deleteLabelVisible && selectedLabel && (
        <Modal
          title="Delete Label"
          onCancel={() => {
            setDeleteLabelVisible(false);
          }}
        >
          <DeleteLabelForm
            afterSubmit={() => {
              setEditLabelVisible(false);
              setDeleteLabelVisible(false);
              fetchLabels();
            }}
            onCancel={() => {
              setDeleteLabelVisible(false);
            }}
            label={selectedLabel}
          />
        </Modal>
      )}
      <LabelList>
        {labels.map((label) => (
          <LabelItem label={label}>
            <CRUDButtonHolder>
              <Link to={`/label/${label.id}`}>
                <ShowButton />
              </Link>
              <EditButton
                onClick={() => {
                  setEditLabelVisible(true);
                  setSelectedLabel(label);
                }}
              />
              <DeleteButton
                onClick={() => {
                  setDeleteLabelVisible(true);
                  setSelectedLabel(label);
                }}
              />
            </CRUDButtonHolder>
          </LabelItem>
        ))}
      </LabelList>
    </Layout>
  );
};

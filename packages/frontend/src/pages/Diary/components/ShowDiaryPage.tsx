import React, { useEffect, useState } from "react";
import { Diary, DiaryDate, DiaryImgUrl, DiaryText, Label } from "./DiaryList";
import Markdown from "react-markdown";
import { Button } from "../../../components/Button";
import styled from "styled-components/macro";
import { DangerButton } from "../../../components/DangerButton";
import { useParams, useNavigate } from "react-router-dom";
import { Modal } from "../../../components/Modal";
import { EditDiaryForm } from "./EditDiaryFrom";
import { DeleteDiaryForm } from "./DeleteDiaryFrom";
import { Layout } from "../../../components/Layout";

const LabelList = styled.ul`
  list-style: none;
  flex-grow: 1;
  font-size: 0.8rem;
  padding: 0;
  margin: 0;

  /* align-self: flex-end; */
  display: flex;
  & > li {
    margin-right: 0.5rem;
    padding: 0.125rem;
    border-radius: 0.25rem;
    background-color: ${(props) => props.theme.colors.primary};
    display: block;
    color: #333;
    margin-top: 5px;
  }
`;

export const ShowDiaryPage = () => {
  const diaryId = useParams().diaryId;
  const [diary, setDiary] = useState<Diary>();
  const [editDiaryVisible, setEditDiaryVisible] = useState(false);
  const [deleteDiaryVisible, setDeleteDiaryVisible] = useState(false);
  const [imgUrlValidate, setImgUrlValidate] = useState(false);
  const navigate = useNavigate();

  const checkIfImageExists = (url: string) => {
    const img = new Image();

    img.src = url;

    if (img.complete) {
      setImgUrlValidate(true);
    } else {
      img.onload = () => {
        setImgUrlValidate(true);
      };

      img.onerror = () => {
        setImgUrlValidate(false);
      };
    }
  };

  const fetchDiary = async () => {
    const diaryRequest = await fetch(`/api/diary/${diaryId}`, {
      headers: { "content-type": "application/json" },
    });
    console.log(diaryRequest);
    if (diaryRequest.status === 200) {
      const diaryJSON = await diaryRequest.json();
      setDiary(diaryJSON.data);
      checkIfImageExists(diaryJSON.data.imgUrl as string);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    (async function () {
      const diaryRequest = await fetch(`/api/diary/${diaryId}`, {
        headers: { "content-type": "application/json" },
      });
      console.log(diaryRequest);
      if (diaryRequest.status === 200) {
        const diaryJSON = await diaryRequest.json();
        setDiary(diaryJSON.data);
        checkIfImageExists(diaryJSON.data.imgUrl as string);
      }
    })();
  }, [diary?.imgUrl, diaryId]);

  return (
    <Layout>
      <h1>{diary && diary.name}</h1>
      <DiaryDate>{diary && diary.createdAt && new Date(diary.createdAt).toLocaleString("de-DE")}</DiaryDate>
      <LabelList>
        {diary &&
          diary.labels &&
          diary.labels.map((label: Label) => {
            return <li key={label.id}>{label.name}</li>;
          })}
      </LabelList>
      <DiaryText>
        <Markdown>{diary ? diary.text : ""}</Markdown>
      </DiaryText>
      <DiaryImgUrl>{imgUrlValidate ? <img src={diary?.imgUrl as string} alt="" /> : diary?.imgUrl}</DiaryImgUrl>
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <Button
          onClick={() => {
            setEditDiaryVisible(true);
          }}
        >
          Edit Diary
        </Button>
        <DangerButton
          onClick={() => {
            setDeleteDiaryVisible(true);
          }}
        >
          Delete Diary
        </DangerButton>
      </div>
      {editDiaryVisible && diary && (
        <Modal
          title="Edit Diary"
          onCancel={() => {
            setEditDiaryVisible(false);
          }}
        >
          <EditDiaryForm
            afterSubmit={() => {
              setEditDiaryVisible(false);
              fetchDiary();
            }}
            onDelete={() => {
              setDeleteDiaryVisible(true);
            }}
            diary={diary}
          />
        </Modal>
      )}
      {deleteDiaryVisible && diary && (
        <Modal
          title="Delete Diary"
          onCancel={() => {
            setDeleteDiaryVisible(false);
          }}
        >
          <DeleteDiaryForm
            afterSubmit={() => {
              setEditDiaryVisible(false);
              setDeleteDiaryVisible(false);
              navigate("/diary");
            }}
            onCancel={() => {
              setDeleteDiaryVisible(false);
            }}
            diary={diary}
          />
        </Modal>
      )}
    </Layout>
  );
};

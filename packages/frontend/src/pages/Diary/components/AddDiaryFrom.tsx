import React, { useState, ChangeEvent, useEffect } from "react";
import { Input } from "../../../components/Input";
import { Button } from "../../../components/Button";
import { Textarea } from "../../../components/Textarea";
import { SelectInput, Option } from "../../../components/SelectInput";
// import { labelContext } from "../../../contexts/LabelContext";

export const AddDiaryForm: React.FC<{ afterSubmit: () => void }> = ({ afterSubmit }) => {
  /* const {
    labels,
    actions: { refetch: refetchLabels },
  } = useContext(labelContext); */
  const [labels, setLabels] = useState<Option[]>([]);
  const refetchLabels = async () => {
    const labelRequest = await fetch("/api/label", {
      headers: {
        "content-type": "application/json",
      },
    });
    const labelJson = await labelRequest.json();
    console.log(labelJson.data);
    setLabels(labelJson.data as Option[]);
  };

  useEffect(() => {
    (async function () {
      await refetchLabels();
    })();
  }, []);
  const [values, setValues] = useState({
    name: "",
    text: "",
    imgUrl: "",
    labels: [] as Option[],
  });
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const TextAreaDidChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(values);

    await fetch("/api/diary", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        ...values,
      }),
    });
    await refetchLabels();
    afterSubmit();
  };
  return (
    <form onSubmit={onSubmitForm}>
      <Input name="name" type="text" label="Name" onChange={fieldDidChange} required />
      <Textarea name="text" label="Text" onChange={TextAreaDidChange} required />
      <Input name="imgUrl" label="Image URL" type="text" onChange={fieldDidChange} />
      <SelectInput
        label="Labels"
        options={labels}
        onChangeSelectedOptions={(options) => {
          console.log("options change", options);
          setValues({ ...values, labels: options });
        }}
      />
      <Button type="submit">Add Diary</Button>
    </form>
  );
};

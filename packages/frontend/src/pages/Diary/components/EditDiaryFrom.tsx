import React, { useState, ChangeEvent, useEffect } from "react";
import { Input } from "../../../components/Input";
import { Button } from "../../../components/Button";
import { Textarea } from "../../../components/Textarea";
import { SelectInput, Option } from "../../../components/SelectInput";
import { Diary } from "./DiaryList";
import { DangerButton } from "../../../components/DangerButton";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styled from "styled-components/macro";
// import { labelContext } from "../../../contexts/LabelContext";

interface EditDiaryFormState {
  name: string;
  text: string;
  imgUrl: string;
  labels: Option[];
}

export const EditDiaryForm: React.FC<{ afterSubmit: () => void; onDelete: () => void; diary: Diary }> = ({ afterSubmit, onDelete, diary }) => {
  /* const {
    labels,
    actions: { refetch: refetchLabels },
  } = useContext(labelContext); */
  const [labels, setLabels] = useState<Option[]>([]);
  const refetchLabels = async () => {
    const labelRequest = await fetch("/api/label", {
      headers: {
        "content-type": "application/json",
      },
    });
    const labelJson = await labelRequest.json();
    console.log(labelJson.data);
    setLabels(labelJson.data as Option[]);
  };

  useEffect(() => {
    (async function () {
      await refetchLabels();
    })();
  }, []);
  const [values, setValues] = useState<EditDiaryFormState>(diary);
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const TextAreaDidChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(values);

    await fetch(`/api/diary/${diary.id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        ...values,
      }),
    });
    await refetchLabels();
    afterSubmit();
  };
  return (
    <form onSubmit={onSubmitForm}>
      <Input name="name" type="text" label="Name" onChange={fieldDidChange} value={values.name} required />
      <Textarea name="text" label="Text" onChange={TextAreaDidChange} value={values.text} required />
      <Input name="imgUrl" label="Image URL" type="text" value={values.imgUrl} onChange={fieldDidChange} />
      <SelectInput
        label="Labels"
        options={labels}
        initialState={{ inputValue: "", selectedOptions: values.labels }}
        onChangeSelectedOptions={(options) => {
          console.log("options change", options);
          setValues({ ...values, labels: options });
        }}
      />
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <Button type="submit">Update Diary</Button>
        <DangerButton type="button" onClick={onDelete}>Delete Diary</DangerButton>
      </div>
    </form>
  );
};

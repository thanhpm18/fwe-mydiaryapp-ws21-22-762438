import React from "react";
import { Button } from "../../../components/Button";
import { Diary } from "./DiaryList";
import { DangerButton } from "../../../components/DangerButton";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styled from "styled-components/macro";
// import { labelContext } from "../../../contexts/LabelContext";

export const DeleteDiaryForm: React.FC<{ afterSubmit: () => void; onCancel: () => void; diary: Diary }> = ({
  afterSubmit,
  onCancel,
  diary,
}) => {
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    await fetch(`/api/diary/${diary.id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    afterSubmit();
  };
  return (
    <form onSubmit={onSubmitForm}>
      <h3>Do you really want to delete this?</h3>
      <h4>Diary: {diary.name}</h4>
      <h5>Created at: {new Date(diary.createdAt).toLocaleString("de-DE")}</h5>
      <div
        css={`
          display: flex;
          flex-direction: row;
        `}
      >
        <Button onClick={onCancel}>Cancel</Button>
        <DangerButton type="submit">Delete Diary</DangerButton>
      </div>
    </form>
  );
};

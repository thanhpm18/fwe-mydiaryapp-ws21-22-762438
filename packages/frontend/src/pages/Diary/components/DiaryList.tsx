import React from "react";
import styled from "styled-components/macro";

export type Label = {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
};

export type Diary = {
  id: number;
  name: string;
  text: string;
  imgUrl: string;
  createdAt: Date;
  updatedAt: Date;
  labels: Label[];
};

const LabelList = styled.ul`
  list-style: none;
  flex-grow: 1;
  font-size: 0.8rem;
  padding: 0;

  align-self: flex-end;
  display: flex;
  & > li {
    margin-right: 0.5rem;
    padding: 0.125rem;
    border-radius: 0.25rem;
    background-color: ${(props) => props.theme.colors.primary};
    display: flex;
    align-items: center;
    justify-content: center;
    color: #333;
    margin-top: 5px;
  }
`;

const DiaryFlex = styled.div`
  display: flex;
  align-items: center;
`;

export const DiaryHighlight = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: none;
  width: 4px;
  background-color: ${(props) => props.theme.colors.primary};
`;

export const DiaryItemStyle = styled.div`
  margin: 0;
  min-height: 3rem;
  position: relative;
  padding: 0.7rem 2rem;
  &:hover {
    ${DiaryHighlight} {
      display: block;
    }
  }
`;
export const DiaryList = styled.ul`
  list-style: none;
  box-shadow: 0 0.125em 0.25em 0 ${(props) => props.theme.colors.shadowColor};
  width: 100%;
  min-width: 380px;
  padding: 0;
  border-radius: 0.5rem;
  background-color: ${(props) => props.theme.colors.listBackgroundColor};
  ${DiaryItemStyle} {
    border-bottom: 1px ${(props) => props.theme.colors.shadowColor} solid;
    &:last-of-type {
      border-bottom: 0;
    }
  }
`;

export const DiaryName = styled.p`
  font-size: 1.1rem;
  font-weight: 500;
  margin: 0;
`;

export const DiaryText = styled.div`
  font-size: 1rem;
  font-weight: lighter;
  padding: 0 5%;
  margin: 0;
  width: 100%;
`;
export const DiaryDate = styled.p`
  margin: 0;
  font-size: 0.8rem;
  color: ${(props) => props.theme.colors.secondaryFontColor};
`;
export const DiaryImgUrl = styled.div`
  margin-bottom: 15px;
  & > img{
    width: 100%;
  }
`;
export type DiaryItemProps = {
  diary: Diary;
  labelInvisible: boolean;
};

export const DiaryItem: React.FC<DiaryItemProps> = ({
  diary: { id, name, text, createdAt, labels, imgUrl },
  labelInvisible,
  children,
}) => {
  return (
    <DiaryItemStyle>
      <DiaryHighlight />
      <DiaryFlex>
        <div
          css={`
            margin-right: 20px;
          `}
        >
          <DiaryName>{name}</DiaryName>
          <DiaryDate>{createdAt && new Date(createdAt).toLocaleString("de-DE")}</DiaryDate>
        </div>
        {children}
      </DiaryFlex>
      {labelInvisible && (
        <LabelList>
          {labels &&
            labels.map((label: Label) => {
              return (
                <li key={label.id}>
                  {label.name}
                </li>
              );
            })}
        </LabelList>
      )}
    </DiaryItemStyle>
  );
};

import React, { ChangeEvent, useEffect, useState } from "react";
import styled from "styled-components/macro";
import { DiaryList, DiaryItem, Diary } from "./components/DiaryList";
import { AddButton } from "./components/AddButton";
import { AddDiaryForm } from "./components/AddDiaryFrom";
import { Modal } from "../../components/Modal";
import { ShowButton } from "../../components/ShowButton";
import { EditButton } from "../../components/EditButton";
import { DeleteButton } from "../../components/DeleteButton";
import { EditDiaryForm } from "./components/EditDiaryFrom";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CSVButtonHolder } from "../../components/CSVButtonHolder";
import { Button } from "../../components/Button";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { SelectInput, Option } from "../../components/SelectInput";
import { DeleteDiaryForm } from "./components/DeleteDiaryFrom";
import { Input } from "../../components/Input";
import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { Layout } from "../../components/Layout";

const CRUDButtonHolder = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0;
  right: 0;
  margin: 15px;
`;
const FilterShowButton = styled.h4`
  cursor: pointer;
  width: fit-content;
  &:hover {
    color: ${(props) => props.theme.colors.primary};
    border-bottom: 1px solid ${(props) => props.theme.colors.primary};
  }
`;

export const DiaryPage = () => {
  const [diaries, setDiaries] = useState<Diary[]>([]);

  const [addDiaryVisible, setAddDiaryVisible] = useState(false);
  const [editDiaryVisible, setEditDiaryVisible] = useState(false);
  const [deleteDiaryVisible, setDeleteDiaryVisible] = useState(false);

  const [selectedDiary, setSelectedDiary] = useState<Diary | null>(null);

  const [filteredDiaries, setFilteredDiaries] = useState<Diary[]>([]);
  // const [filtered, setFiltered] = useState(false);
  const [filterShow, setFilterShow] = useState(false);
  const [labels, setLabels] = useState<Option[]>([]);
  const [startDate, setStartDate] = useState<Date | null>(null);

  const [filterValues, setFilterValues] = useState({
    name: "",
    text: "",
    date: null as Date | null,
    labels: [] as Option[],
  });

  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFilterValues({ ...filterValues, [e.target.name]: e.target.value });
  };

  const filterDiaries = async () => {
    setFilteredDiaries(
      diaries
        .filter((diary) => {
          const diaryDate = new Date(diary.createdAt);
          return (
            (diaryDate.getDate() === filterValues.date?.getDate() &&
              diaryDate.getMonth() === filterValues.date?.getMonth() &&
              diaryDate.getFullYear() === filterValues.date?.getFullYear()) ||
            !filterValues.date
          );
        })
        .filter(
          (diary) =>
            diary.labels.some((label) => filterValues.labels.some((filterLabel) => filterLabel.name === label.name)) ||
            !filterValues.labels.length
        )
        .filter(
          (diary) => diary.name.toLowerCase().includes(filterValues.name.toLowerCase()) || !filterValues.name.length
        )
        .filter(
          (diary) => diary.text.toLowerCase().includes(filterValues.text.toLowerCase()) || !filterValues.text.length
        )
    );
  };

  const onFilterSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(filterValues);
    filterDiaries();
  };

  const csvData = filteredDiaries.map((item) => ({
    id: item.id,
    name: item.name,
    text: item.text,
    imgUrl: item.imgUrl,
    createdAt: item.createdAt,
    updatedAt: item.updatedAt,
    labels: item.labels.map((label) => label.name),
  }));
  const csvHeaders = [
    { label: "ID", key: "id" },
    { label: "Name", key: "name" },
    { label: "Text", key: "text" },
    { label: "Image Url", key: "imgUrl" },
    { label: "Created At", key: "createdAt" },
    { label: "Updated At", key: "updatedAt" },
    { label: "Labels", key: "labels" },
  ];

  const fetchDiaries = async function () {
    const diaryRequest = await fetch("/api/diary", {
      headers: { "content-type": "application/json" },
    });
    console.log(diaryRequest);
    if (diaryRequest.status === 200) {
      const diaryJSON = await diaryRequest.json();
      setDiaries(diaryJSON.data);
      setFilteredDiaries(diaryJSON.data);
    }
  };

  useEffect(() => {
    (async function () {
      window.scrollTo(0, 0);
      const diaryRequest = await fetch("/api/diary", {
        headers: { "content-type": "application/json" },
      });
      console.log(diaryRequest);
      if (diaryRequest.status === 200) {
        const diaryJSON = await diaryRequest.json();
        setDiaries(diaryJSON.data);
        setFilteredDiaries(diaryJSON.data);
      }
    })();
  }, []);

  const refetchLabels = async () => {
    const labelRequest = await fetch("/api/label", {
      headers: {
        "content-type": "application/json",
      },
    });
    const labelJson = await labelRequest.json();
    console.log(labelJson.data);
    setLabels(labelJson.data as Option[]);
  };

  useEffect(() => {
    (async function () {
      await refetchLabels();
    })();
  }, []);

  return (
    <Layout>
      <div
        css={`
          display: flex;
          flex-direction: column;
          width: 100%;
        `}
      >
        <div>
          <h1>Diary</h1>
        </div>
        <div
          css={`
            flex: 1;
            display: flex;
          `}
        >
          <AddButton
            onClick={() => {
              setAddDiaryVisible(true);
            }}
          />
          <CSVButtonHolder>
            <CSVLink
              data={csvData}
              headers={csvHeaders}
              filename={"diaries.csv"}
              separator={";"}
              css={`
                all: unset;
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
              `}
            >
              <FontAwesomeIcon icon={["fas", "file-csv"]} size="2x" />
            </CSVLink>
          </CSVButtonHolder>
        </div>
      </div>
      <FilterShowButton
        onClick={() => {
          setFilterShow(filterShow ? false : true);
        }}
      >
        Filter <FontAwesomeIcon icon={filterShow ? faAngleUp : faAngleDown} />
      </FilterShowButton>
      {filterShow && (
        <form onSubmit={onFilterSubmitForm}>
          <Input name="name" type="text" label="Name" onChange={fieldDidChange} value={filterValues.name} />
          <Input name="text" type="text" label="Text" onChange={fieldDidChange} value={filterValues.text} />
          <div
            css={`
              display: flex;
              flex-direction: row;
              width: 100%;
            `}
          >
            <div
              css={`
                width: 210px;
              `}
            >
              <DatePicker
                selected={startDate}
                placeholderText="Click to select a date"
                showPopperArrow={false}
                dateFormat="yyyy.MM.dd"
                isClearable={true}
                onChange={(selectedDate: Date) => {
                  setStartDate(selectedDate);
                  setFilterValues({ ...filterValues, date: selectedDate });
                }}
              />
            </div>
            <div
              css={`
                width: calc(100% - 210px);
              `}
            >
              <SelectInput
                label="Labels"
                options={labels}
                initialState={{ inputValue: "", selectedOptions: filterValues.labels }}
                onChangeSelectedOptions={(options: any) => {
                  console.log("options change", options);
                  setFilterValues({ ...filterValues, labels: options });
                }}
              />
            </div>
          </div>
          <Button type="submit">
            Filter <FontAwesomeIcon icon={["fas", "filter"]} size="lg" />
          </Button>
        </form>
      )}
      {addDiaryVisible && (
        <Modal
          title="Create new Diary"
          onCancel={() => {
            setAddDiaryVisible(false);
          }}
        >
          <AddDiaryForm
            afterSubmit={() => {
              setAddDiaryVisible(false);
              fetchDiaries();
            }}
          />
        </Modal>
      )}
      {editDiaryVisible && selectedDiary && (
        <Modal
          title="Edit Diary"
          onCancel={() => {
            setEditDiaryVisible(false);
          }}
        >
          <EditDiaryForm
            afterSubmit={() => {
              setEditDiaryVisible(false);
              fetchDiaries();
            }}
            onDelete={() => {
              setDeleteDiaryVisible(true);
            }}
            diary={selectedDiary}
          />
        </Modal>
      )}
      {deleteDiaryVisible && selectedDiary && (
        <Modal
          title="Delete Diary"
          onCancel={() => {
            setDeleteDiaryVisible(false);
          }}
        >
          <DeleteDiaryForm
            afterSubmit={() => {
              setEditDiaryVisible(false);
              setDeleteDiaryVisible(false);
              fetchDiaries();
            }}
            onCancel={() => {
              setDeleteDiaryVisible(false);
            }}
            diary={selectedDiary}
          />
        </Modal>
      )}
      <DiaryList>
        {filteredDiaries.map((diary) => (
          <DiaryItem diary={diary} labelInvisible={true}>
            <CRUDButtonHolder>
              <Link to={`/diary/${diary.id}`}>
                <ShowButton />
              </Link>
              <EditButton
                onClick={() => {
                  setEditDiaryVisible(true);
                  setSelectedDiary(diary);
                }}
              />
              <DeleteButton
                onClick={() => {
                  setDeleteDiaryVisible(true);
                  setSelectedDiary(diary);
                }}
              />
            </CRUDButtonHolder>
          </DiaryItem>
        ))}
      </DiaryList>
    </Layout>
  );
};

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import styled from "styled-components/macro";
import { Layout } from "../components/Layout";
import { Modal } from "../components/Modal";
import { AddDiaryForm } from "./Diary/components/AddDiaryFrom";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  height: auto;
`;

const Title = styled.h1`
  font-size: xxx-large;
`;

const CreateButtonWrapper = styled.div`
  align-items: center;
  text-align: center;
  margin: auto;
  width: 380px;
  height: 205px;
  border-radius: 15px;
  border: 2px solid ${(props) => props.theme.colors.primary};
`;

const CreateButton = styled.button`
  all: unset;
  color: ${(props) => props.theme.colors.secondaryFontColor};
  margin-top: -10px;
  font-size: 6rem;
  line-height: 8rem;
  transition-duration: 0.3s;
  &:hover {
    font-size: 8rem;
    filter: brightness(2);
    transition-duration: 0.3s;
  }
`;

export const HomePage = () => {
  const [addDiaryVisible, setAddDiaryVisible] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Layout>
      <Wrapper>
        <Title>Wellcome to My Diary App</Title>
        <CreateButtonWrapper>
          <h2>Write a new Diary</h2>
          <CreateButton
            onClick={() => {
              setAddDiaryVisible(true);
            }}
          >
            <FontAwesomeIcon icon={["fas", "plus-square"]} />
          </CreateButton>
        </CreateButtonWrapper>
      </Wrapper>
      {addDiaryVisible && (
        <Modal
          title="Create new Diary"
          onCancel={() => {
            setAddDiaryVisible(false);
          }}
        >
          <AddDiaryForm
            afterSubmit={() => {
              setAddDiaryVisible(false);
              navigate("/diary");
            }}
          />
        </Modal>
      )}
    </Layout>
  );
};

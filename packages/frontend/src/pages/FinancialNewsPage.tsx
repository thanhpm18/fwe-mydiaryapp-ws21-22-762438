import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { Layout } from "../components/Layout";

export type FinancialNews = {
  BTC_EUR: number;
  BTC_USD: number;
  ETH_EUR: number;
  ETH_USD: number;
  EUR_USD: number;
  EUR_VND: number;
  timestamp: number;
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
`;

const StyledTable = styled.table`
  border: 1px solid ${(props) => props.theme.colors.primary};
  width: 80%;
  min-width: 380px;
  margin-bottom: 15px;
  height: 100px;
  table-layout: fixed;
`;

const StyledCell = styled.td`
  border: 1px solid ${(props) => props.theme.colors.primary};
  width: 100/3%;
  text-align: center;
`;

const StyledCellHeader = styled.th`
  border: 1px solid ${(props) => props.theme.colors.primary};
  width: 100/3%;
  text-align: center;
`;

export const FinancialNewsPage = () => {
  const [financial, setFinancial] = useState<FinancialNews>();

  const fetchNews = async function () {
    const financialRequest = await fetch(`/api/financial`, {
      method: "GET",
      headers: { "content-type": "application/json" },
    });
    console.log(financialRequest);
    if (financialRequest.status === 200) {
      const financialJSON = await financialRequest.json();
      setFinancial(financialJSON);
    }
  };

  useEffect(() => {
    (async function () {
      window.scrollTo(0, 0);
      await fetchNews();
    })();
  }, []);

  return (
    <Layout>
      <Wrapper>
        <h1>Financial News {new Date(Number(financial?.timestamp)).toLocaleString("de-DE")}</h1>
        <StyledTable>
          <tr>
            <StyledCellHeader></StyledCellHeader>
            <StyledCellHeader>USD</StyledCellHeader>
            <StyledCellHeader>VND</StyledCellHeader>
          </tr>
          <tr>
            <StyledCellHeader>EUR</StyledCellHeader>
            <StyledCell>{financial?.EUR_USD}</StyledCell>
            <StyledCell>{financial?.EUR_VND}</StyledCell>
          </tr>
        </StyledTable>

        <StyledTable>
          <tr>
            <StyledCellHeader></StyledCellHeader>
            <StyledCellHeader>USD</StyledCellHeader>
            <StyledCellHeader>EUR</StyledCellHeader>
          </tr>
          <tr>
            <StyledCellHeader>BTC</StyledCellHeader>
            <StyledCell>{financial?.BTC_USD}</StyledCell>
            <StyledCell>{financial?.BTC_EUR}</StyledCell>
          </tr>
          <tr>
            <StyledCellHeader>ETH</StyledCellHeader>
            <StyledCell>{financial?.ETH_USD}</StyledCell>
            <StyledCell>{financial?.ETH_EUR}</StyledCell>
          </tr>
        </StyledTable>
      </Wrapper>
    </Layout>
  );
};

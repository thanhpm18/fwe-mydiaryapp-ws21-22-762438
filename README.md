# FWE-WS21-22-762438: My Diary App

## Setup backend

Create environment file

- `cp ./packages/backend/.env.example ./packages/backend/.env`

Install npm packages on local

- `npm install`

Start containers

- `docker-compose up` / `docker-compose up -d`

Install npm packages on docker container

- `docker-compose exec backend npm install`

Sync database schema

- `docker-compose exec backend npm run typeorm schema:sync`

Test backend

- `docker-compose exec backend npm run test`

## Setup frontend

Open another Terminal and change directory to frontend

- `cd packages/frontend`

Install npm packages on local

- `npm install`

Start frontend app

- `npm start` / `yarn start`

---

## Documentation

### Features

- Creeate, Read, Update, Delete a diary

- Creeate, Read, Update, Delete a label

- Add or delete a label to/from a diary

- Filter diary list by Name, Text, Created Date and/or Label

- Download list of diary or label in format CSV file

- Get some financial news from external API (currency and crypto-currency rates informations)

  - Currency rates API: [`https://freecurrencyapi.net/`](https://freecurrencyapi.net/)

  - Crypto-currency rates API: [`https://api.alternative.me/`](https://api.alternative.me/)

### Routes

#### Backend

- Get a JSON list of all diaries

> `http://localhost:3001/api/diary`

- Get a JSON objecct of a diary by ID

> `http://localhost:3001/api/diary/:diaryId`

- Create a new diary

> `http://localhost:3001/api/diary`
>
> - Method: POST
>
> - Body: JSON format

- Update a diary

> `http://localhost:3001/api/diary/:diaryId`
>
> - Method: PATCH
>
> - Body: JSON format

- Delete a diary

> `http://localhost:3001/api/diary/:diaryId`
>
> - Method: DELETE

- Get a JSON list of all labels

> `http://localhost:3001/api/label`

- Get a JSON objecct of a label by ID

> `http://localhost:3001/api/label/:labelId`

- Create a new label

> `http://localhost:3001/api/label`
>
> - Method: POST
>
> - Body: JSON format

- Update a label

> `http://localhost:3001/api/label/:labelId`
>
> - Method: PATCH
>
> - Body: JSON format

- Delete a label

> `http://localhost:3001/api/label/:labelId`
>
> - Method: DELETE

- Get a financial data in JSON format

> `http://localhost:3001/api/financial`

#### Frontend

- Homepage: `http://localhost:3000/`

- Diary list page: `http://localhost:3000/diary`

- Diary detail page: `http://localhost:3000/diary/:diaryId`

- Label list page: `http://localhost:3000/label`

- Label detail page: `http://localhost:3000/label/:labelId`

- Financial News page: `http://localhost:3000/financial`

- Click on CSV Button on Diary or Label list page to download CSV file
